# rockstor-opensuse

Ansible playbook to kick a Rockstor install on OpenSUSE Leap 15.X

1. Follow the "Installer options" instructions here: https://forum.rockstor.com/t/built-on-opensuse-dev-notes-and-status/5662
   - Use at least a 20 GB system disk, this auto enables boot to snapshot functionality.
   - Server (non transactional)
   - Default partitioning
   - Skip user creation (then only root password to enter and leaves all additional users as Rockstor managed)
     - Optional, load ssh key from thumb drive for root user access via ansible
   - Click "Software" heading, then uncheck "AppArmor", then click Next or Apply (I forget which it is specifically)
   - Disable Firewall
   - Leave SSH service enabled
   - Switch to NetworkManager
2. After the install and initial reboot, make sure you can log in using the previously created SSH Key, or create one now and load it via `ssh-copy-id`
3. Create an inventory file for ansible, something like the following in `hosts.yml`:
    ```yaml
    ---
    all:
        hosts:
            rockstor:
                # Use your server's IP
                ansible_ssh_host: 192.168.88.100
                # Use the path to your local SSH Private Key
                ansible_ssh_private_key_file: /home/youruser/.ssh/id_rsa_ansible
                ansible_user: root
    ```
4. Run the ansible playbook:
    ```sh
    ansible-playbook -i hosts.yml main.yml
    ```
5. Enjoy your OpenSUSE-based Rockstor!